/ // console.log("Hello World!");

// // [SECTION] Assignment Operator

// // Basic Assignment Operator (=)
// // it assigns the value of the right operand to a variable.
// let assignmentNumber = 8;

// // Addition Assignment Operator
// // 8 					8 + 2
// // assignmentNumber = assignmentNumber + 2;
// // console.log("Result of the Addition Assignment Operator:" +assignmentNumber);

// // Shorthand method
// assignmentNumber += 2;
// console.log("Result of the Addition Assignment Operator:" +assignmentNumber);

// // Subtraction(-=)/Multiplication(*=)/Division(/=) Assignment Operator

// // Subtraction
// assignmentNumber -= 2;
// console.log("Result of the Subtraction Assignment Operator:" +assignmentNumber);

// // Multiplication
// assignmentNumber *= 2;
// console.log("Result of the Multiplication Assignment Operator:" +assignmentNumber);

// // Division
// assignmentNumber /= 2;
// console.log("Result of the Division Assignment Operator:" +assignmentNumber);

// // [SECTION] Arithmetic Operators

// let x = 21;
// let y = 9;

// // Addition
// let sum = x + y;
// console.log("Result of addition operator: " +sum);

// // Subtration
// let difference = x - y;
// console.log("Result of subtration operator: " +difference);

// // Multiplication
// let product = x * y;
// console.log("Result of multiplication operator: " +product);

// // Division
// let quotient = x / y;
// console.log("Result of divsion operator: " +quotient);

// // Remainder
// let modulo = x % y;
// console.log("Result of the modulo operator: " +modulo);

// // Multiple Operators and Parenthesis

// // PEMDAS - Parentheis, Exponent, Multiplication, Division, Addition, & Subtraction Rule

// let mdas = 1 + 2 - 3 * 4 / 5;
// console.log("Result of the mdas operation: " +mdas.toFixed(2));
// /*
// 	1. 3 * 4 = 12
// 	2. 12 / 5 = 2.4
// 	3. 1 + 2 = 3
// 	4. 3 - 2.4 = 0.6
// */

// let pemdas = (1 + (2-3)) * (4/5);
// console.log("Result of pemdas operation: "+pemdas.toFixed(2));

// // Increment and Decrement
// // This operators add or subtract values by 1 and reassign the value of the variable where the increment/decrement was applied to.

// let z = 1;
// console.log("Result of z before increment/decrement: "+z);

// // Pre-increment
// // The value of "z" is added by one before storing it in the "increment" variable.
// 				//1 ++ ->2
// let increment = ++z;
// console.log("Result of the pre-increment: " +increment);
// console.log("Result of pre-increment: " +z);

// // Post increment
// // The value of "z" is stored in the "increment" variable before the value of "z" is increased by one.
// 			// 2
// increment = z++;
// console.log("Result of the post increment: " +increment);
// console.log("Result of post increment: " +z);

// // Pre-decrement
// let decrement = --z;
// console.log("Result of the pre-decrement: " +decrement);
// console.log("Result of the pre-decrement: " +z);

// // Post decrement
// decrement = z--;
// console.log("Result of the post-decrement: " +decrement);
// console.log("Result of the post-decrement: " +z);

// // [SECTION] Type Coercion
// // automatic or implicit conversion of values from one data type to another.

// // example of coercion
// let numA = '10';
// let numB = 12;

// let coercion = numA + numB;
// console.log(coercion);
// // typeof it is used to show the data type of a specific variable.
// console.log(typeof coercion);

// // example of nonCoercion
// let numC = 16;
// let numD = 14;

// let nonCoercion = numC + numD;
// console.log(nonCoercion);
// console.log(typeof nonCoercion);

// // boolean "true" is also associated with the value of 1.
// let numE = true + 1;
// console.log(numE);

// // boolean "false" is also associated with the value of 0.
// let numF = false + 1;
// console.log(numF);

// // [SECTION] Comparison Operators

// // (=) used for assignment
// let juan = 'juan';

// // Equality Operator (==)
// // checks wheter the operands are equal/have the same content.
// // Attempts to CONVERT AND COMPARE operands of different data type.
// // Returns a Boolean

// console.log(1 == 1);
// console.log(1 == 2);
// console.log(1 == '1');
// console.log(0 == false);
// console.log(1 == true);
// // comparision with string is case sensitive.
// console.log('juan' == 'juan');
// // compares a string with the variable "juan" declared above.
// console.log(juan == 'juan');

// // Inequality operator (!=)
// // checks whether the operands are not equal/have different content.
// console.log(1 != 1);
// console.log(1 != 2);
// console.log(1 != '1');
// console.log(0 != false);
// console.log(1 != true);

// // Strict Equality Operator (===)
// // checks wheter the operands are equal/have the same content.
// // also COMPARES the data type of 2 values.
// console.log(1 === 1);
// console.log(1 === 2);
// console.log(1 === '1');
// console.log(0 === false);
// console.log(1 === true);
// // compares the value of the variable to the other operand.
// console.log(juan === 'juan');

// // Strict Inequality Operator (!==)
// // checks whether the operands are not equal/have different content.
// // also COMPARES the data type of 2 values.
// console.log(1 !== 1);
// console.log(1 !== 2);
// console.log(1 !== '1');
// console.log(0 !== false);
// console.log(1 !== true);

// // <, >, <=, >=
// console.log("Less than and greater than");
// console.log(4 < 6); //true
// console.log(2 > 8); //false
// console.log(9 > 9); //false
// // greater than or equal
// console.log(9 >= 9); //true
// console.log(10 <= 15); //true

// // [SECTION] Logical Operators

// let isLegalAge = true;
// let isRegistered = false;

// // Logical AND Operator (&& - Double ampersand);
// // Return true if all operands are true. (both operands are true)
// let allRequirementsMet = isLegalAge && isRegistered;
// console.log("Result of logical AND operator:" +allRequirementsMet);

// // AND operator
// console.log(true && true); //true
// console.log(true && false); //false
// console.log(false && true); //false
// console.log(false && false); //false

// // Logical OR Operator (|| - Double Pipe)
// // Returns true if one of the operands are true. (atleast one operand is true)
// let someRequirementsMet = isLegalAge || isRegistered;
// console.log("Result of logical OR operator:" +someRequirementsMet);

// //OR operator
// console.log(true || true);
// console.log(true || false);
// console.log(false || true);
// console.log(false || false);

// // Logical NOT operator (! - Exclamation Point)
// // Returns the opposite value.
// let someRequirementsNotMet = !isRegistered;
// console.log("Result of logical NOT operator:" +someRequirementsNotMet);

// // NOT operator
// console.log(!true);
// console.log(!false);

// // Multiple logical operators
// // Left to right precedence rule
// let isQualified = isLegalAge && isRegistered || isLegalAge;
// /*
// 	Highest priority NOT
// 	Lowest priority OR
// 	= true && false //false
// 	= false || true //true
// 	= true

// */
// console.log("Result of isQualified:" +isQualified);

// isQualified = !(isLegalAge && isRegistered) && isRegistered

// /*
// 	= true && false //false
// 	= !false //true
// 	= true && false
// 	= false
// */
// console.log("Result of isQualified:" +isQualified);

// [SECTION] if, else if, and else Statement

/*
	Syntax: 
	if (condition){
		//Code block
	}
	else if (condition){
		//execute this code block if the previous value result to false.
	}
	else{
		//Code block if not satisfied.
	}

*/

let numG = 1;

// if Statement
// Executes a statement if a specified contion is true.
// 1 < 0 - false
if(numG < 0){
	console.log("Hello");
}
// else if Clause
// executes a statement if previous conditions are false and if the specified/current condition is true.
		//1 == 1
else if(numG == 1){
	console.log("My");
}
// else Statement
// Executes a statement if all other conditions are false
else {
	console.log("World");
}

// if, else if, and else Statement with function

/*
	Scenario: We want to determine intensity of a typhoon based on its wind speed.

		Not a Typoon - Wind speed is less than 30. (windSpeed < 30)
		Tropical Depression - Wind speed is less than or equal to 61. (windSpeed <= 61)
		Tropical Storm - Wind speed is between 62 to 88. (windSpeed >= 62 && windSpeed <= 88)
		Severe Tropical Storm - Wind speed is between 89 to 117. (windSpeed >= 89 && windSpeed <= 117)
		Typoon - Wind speed is greater than or equal to 118. (windSpeed >= 118)

*/

let message = "No message.";
console.log(message);


function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a Typhoon";
	}
	else if(windSpeed <= 61){
		return "Tropical Depression detected."
	}
			//true    		&&		false
	else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical Storm detected."
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return "Severe Tropical Storm detected."
	}
	else{
		return "Typhoon is detected."
	}
}

// Returns the string to the variable message that invoked it.
message = determineTyphoonIntensity(110);
console.log(message);

// [SECTION] Truthy and Falsy
// a "truthy" values ia a values is considered true when encountered in a Boolean context.

// Examples of Truthy - true | 1 | []
// let result = [];

// if (result){
// 	console.log("Truthy");
// }

// Examples of Falsy - false | 0 | undefined | null | ""
let result = 1;

if (result){
	console.log("Truthy");
}
else{
	console.log("Falsy");
}

// [SECTION] Ternary Operator
// Single statement execution
let ternaryResult = (18 < 18) ? true : false;
console.log("Result of Ternary Operator:" +ternaryResult);

// prompt()
// create a pop-up message in the bnrowser that can be used to gather user input.

/*
	Create a conditional statement that will check the age of a person and will determine if he/she is qualified to vote. If the person's age is 18 years old and above print the message "You're qualified to vote!" else print "Sorry, You're too young to vote."

	Sample Output:
	//prompt
	Enter your age: 17;

	//Browser Console:
	Sorry, You're too young to vote.
*/
// parseInt()
// Converts a string variable to integer/number.

function isLegalAge(){
	return "You're qualified to vote!";
}

function isUnderAge(){
	return "Sorry, You're too young to vote.";
}

let age = parseInt(prompt("What is your age?"));
console.log(typeof age);

// if(age >= 18){
// 	// it displays an alert box with a message and an OK button.
// 	alert(isLegalAge());
// }
// else{
// 	alert(isUnderAge());
// }

let isVoter = (age >= 18) ? isLegalAge() : isUnderAge();
alert("Result of Ternary Operator " +isVoter);

// [SECTION] Switch Case
// can be used as an alternative to an "if, else if, and else" statement where data to be used in the condition is of an expected output.
/*

	Syntax:
		switch (expression){
			case value:
				statement;
				break;

			default:
				statement;
				brea;
		}
*/

// toLowerCase() method that will change the input received from the prompt into all lowercase letter.
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case "monday":
		console.log("The color of the day is Red");
		break;
	case "tuesday":
		console.log("The color of the day is Orange");
		break;
	case "wednesday":
		console.log("The color of the day is Yellow");
		break;
	case "thursday":
		console.log("The color of the day is Green");
		break;
	case "friday":
		console.log("The color of the day is Blue");
		break;
	case "saturday":
		console.log("The color of the day is Indigo");
		break;
	case "sunday":
		console.log("The color of the day is Pink");
		break;
	default:
		alert("Please input a valid day!");
		break;
}



