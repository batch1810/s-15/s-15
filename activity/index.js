
/*3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
If the total of the two numbers is less than 10, add the numbers <
If the total of the two numbers is 10 - 20, subtract the numbers
If the total of the two numbers is 21 - 29 multiply the numbers
If the total of the two numbers is greater than or equal to 30, divide the numbers
*/

// 4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.

let num1 = parseInt(prompt("Provide a number"));
let num2 = parseInt(prompt("Provide another number"));
let sum = num1+num2;
let result;

if(sum<10){
	console.warn("The sum of two numbers is " +sum);
}
else if(sum >= 10 && sum <= 20){

	result = num2-num1;
	alert("The difference of two numbers is " +result);
}
else if(sum >= 21 && sum <= 29){

	result = num2*num1;
	alert("The product of two numbers is " +result);
}
else if(sum >= 30){

	result = num2/num1;
	alert("The dividend of two numbers is " +result);
}


// 5. Prompt the user for their name and age and print out different alert messages based on the user input:
// a.If the name OR age is blank(“”), print the message are you a time traveler?
// b. If the name AND age is not blank, print the message with the user’s name and age.


let name =prompt("What is your name");
let age = prompt("What is your age?");

if(name == "" || age ==""){
	alert("Are you a time traveler");

}
else {
	alert("Hello "+ name+ ". " + "Your age is " + age);
}

// 6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
// 18 or greater, print an alert message saying You are of legal age.
// 17 or less, print an alert message saying You are not allowed here.

function isLegalAge(age){

	if(parseInt(age) >= 18){
		alert("You are of legal age");

	}
	else{
		alert("You are not allowed here");
	}

}

isLegalAge(age);


// 7. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
// 18 - print the message You are now allowed to party.
// 21 - print the message You are now part of adult society.
// 65 - print the message We thank you for your contribution to society.
//  Any other value - print the message Are you sure you're not an alien?
// 8. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message


switch(parseInt(age)){
	case 18:
		alert("You are now allowed to party");
		break;
	case 21:
		alert("You are now part of adult society");
		break;
	case 65:
		alert("We thank you for your contribution to society");
		break;
	default:
		alert("Are you sure you're not an alien");
		break;
}

// 8. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message


try{
	alert(isLegalag(18));
}
catch (error){

	console.warn(error.message);
}
